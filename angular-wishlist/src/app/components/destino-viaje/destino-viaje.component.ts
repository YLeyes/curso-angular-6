//Dentro de import se coloca "input" xa que la reconozca externa
import { Component, OnInit, Input, HostBinding, EventEmitter, Output} from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { VoteDownAction, VoteUpAction } from '../../models/destinos-viajes-state.model';
import { DestinoViaje } from '../../models/destino-viaje.model';

//Se importa las animaciones
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  //Se agrega la animacion
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ]
  )]
})
export class DestinoViajeComponent implements OnInit {
//Colocamos una variable con @input
  @Input() destino: DestinoViaje;
  @Input('idx') position: number;
  //Agregamos "Hostbinding" xa vincular los atributos
  @HostBinding('attr.class') cssClass = "col-md-4"
  //Se declara la propiedad "clicked"//
  @Output() clicked: EventEmitter<DestinoViaje>;

  //Se crea el "wrap" o encapsulador//
  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }
  //Se crea un metodo "ir"//
  ir() {
    this.clicked.emit(this.destino);
    return false;
  }
  //Se agregan botones Up y Down
  voteUp() {
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }
  voteDown() {
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }

}
