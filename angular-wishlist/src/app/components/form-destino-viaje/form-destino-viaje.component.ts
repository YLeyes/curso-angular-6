import { Component, OnInit, Output, EventEmitter, Input,forwardRef, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Event } from '@angular/router';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';
import { DestinoViaje } from '../../models/destino-viaje.model';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  //Agregamos el Evento Output
  @Output() onItemAdded: EventEmitter<DestinoViaje>
  //Fin del Evento

  //Inicializamos el Form Group
  fg: FormGroup;
  //Fin del form group

  //Agregamos Validador Personalizado
  minLongitud= 4;
  //Fin validador

  //Agregamos SearchResults
  searchResults: string[];


  constructor(fb:FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {

    //inicializamos el evento
    this.onItemAdded= new EventEmitter();
    //vinculacion con tag html
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametrizable(this.minLongitud)
      ])],
      url: ['']
    });
    //Agregamos un Observable a la consola de los cambios realizados en el formulario
    this.fg.valueChanges.subscribe((form: any)=>{
      console.log('cambio en el formulario: ', form);
    });
  }

  ngOnInit(): void {
    //Para Autocompletar
    const elemNombre = <HTMLInputElement>document.getElementById("nombre");
    fromEvent(elemNombre, 'input')
    .pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 3),
      debounceTime(120),
      distinctUntilChanged(),
      switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
    ) .subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);

  }

  guardar(nombre: string, url: string): boolean {
    const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  //Agregamos los Validadores
  nombreValidator(control: FormControl): { [s: string]: boolean } {
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 4) {
      return { invalidNombre: true };
    }
    return null;
  }

  //Agregamos un validador Parametrizable
  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): {[ s: string]: boolean } | null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong) {
        return { minLongNombre: true };
      }
      return null;
    }
  }

}
