import { v4 as uuid } from 'uuid';
export class DestinoViaje {
  //Para marcarlos como elegido//
  private selected: boolean;
  //Se agregan los servicios del sitio//
  public servicios: string[];
  id = uuid();

  //Se crea un atajo con "public"//
  constructor(public nombre: string, public u: string, public votes: number = 0) {
    this.servicios = ['Alojamiento', 'Buceo', 'Spa'];
  }
  //Se agrega los metodos//
  isSelected(): boolean {
    return this.selected;

  }
  setSelected(s: boolean) {
    this.selected = s;
  }
  //Se declara los botones de votar
  voteUp(): any {
    this.votes++;
  }
  voteDown(): any {
    this.votes--;
  }
}
