import { BehaviorSubject, Subject } from 'rxjs';
import { DestinoViaje } from './destino-viaje.model';

//redux
import { Injectable, Inject, forwardRef } from '@angular/core';
import { tap, last } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { Store } from '@ngrx/store';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destinos-viajes-state.model';
import { AppState, APP_CONFIG, AppConfig, db, } from './../app.module';
import { HttpRequest, HttpClientModule, HttpHeaders, HttpClient, HttpResponse } from '@angular/common/http';

@Injectable()
export class DestinosApiClient {
  destinos: DestinoViaje[] = [];
  /*SE ELIMINA ESTO XA EVITAR CONFLICTOS
  current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);*/
  constructor(
    private store: Store<AppState>,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
    private http: HttpClient
  ) {
    this.store
    .select(state => state.destinos )
    .subscribe((data) => {
      console.log("destinos sub store");
      console.log(data);
      this.destinos = data.items;
    });
    this.store
    .subscribe((data) => {
      console.log("all store");
      console.log(data);
    });

  }


	add(d: DestinoViaje){
	  const headers: HttpHeaders = new HttpHeaders({ 'X-API-TOKEN': 'token-seguridad' });
    const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if (data.status === 200) {
        this.store.dispatch(new NuevoDestinoAction(d));
        const myDb = db;
        myDb.destinos.add(d);
        console.log('todos los destinos de la db!');
        myDb.destinos.toArray().then(destinos => console.log(destinos))
      }
    });
  }
  getAll(): DestinoViaje[] {
	  return this.destinos;
  }
  getById(id: String): DestinoViaje {
    return this.destinos.filter((d) => { return d.id.toString() === id; })[0];
  }
  //~Aqui se invoca al servidor
  elegir (d: DestinoViaje) {
    /*
    this.destinos.forEach(x => x.setSelected(false));
    d.setSelected(true);
    this.current.next(d);*/
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
  //Agregamos el metodo suscribir
  /*subscribeOnChange(fn) {
    this.current.subscribe(fn);
  }*/

}

